# /etc/profile.d/hadoop.sh

# other variables are set in default hadoo-env.sh

export PATH="$PATH:/opt/hadoop/hadoop-3.2.1/sbin:/opt/hadoop/hadoop-3.2.1/bin"
export HADOOP_CONF_DIR=/opt/hadoop/hadoop-3.2.1/etc/hadoop
