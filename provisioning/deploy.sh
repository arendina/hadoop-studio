#!/bin/bash

#
# Deploy the cluster
#
# to fast check gcloud options you can go on 
# Console > Compute Engine > Create Instance
# Click at the end of the page to have the list of the command line options
# 

servers='master worker-1 worker-2 worker-3'

project='hadoopstudio'
machine_name='master'
zone='us-central1-a'
machine_type='e2-medium'
service_account='' # service account that you created
image='debian-9-stretch-v20201014'
image_project='debian-cloud'

for machine_name in $servers; do

    boot_disk_device_name=$machine_name
    gcloud beta compute \
    --project=$project instances create $machine_name \
    --zone=$zone \
    --machine-type=$machine_type \
    --subnet=default \
    --network-tier=PREMIUM \
    --metadata=ssh-keys=
    --maintenance-policy=MIGRATE \
    --service-account=$service_account \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --image=$image \
    --image-project=$image_project \
    --boot-disk-size=10GB \
    --boot-disk-type=pd-standard \
    --boot-disk-device-name=$boot_disk_device_name \
    --reservation-affinity=any

done

