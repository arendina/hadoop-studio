# Instruction to deploy clusters on Google Cloud

## Create a Project

- Navigate to https://console.cloud.google.com.
- Click on the project selector dropdown
- Select Create Project
- Give it an appropriate unique name.

Save the Project ID.

## Create and authorize a VM to use the Cloud SDK

### Create a service account

Console: Products and Services > IAM & Admin > Service accounts
Click on [Create Service Account]

| Property | Value             |
|----------|-------------------|
| Name:    | management        |
| Role:    | Project -> Editor |

Select Furnish a new private key with Key type JSON.
Click Create
This will download a JSON key file. You will need to find this key file and open it in a text editor to make a copy of it on the VM in the next step.

### Create a key on your desktop (optional)

You can login on the management machine via google cloud interface or if you have a ssh client you can use your own machine:
```
$ ssh-keygen -t ed25519 -C "username" -f ~/.ssh/gcloud
$ ssh-add ~/.ssh/gcloud
```

### Create a VM Instance

You are going to create the VM with a persistent disk that remains when the VM is deleted. This disk will be used to create the pattern you will use later to create a cluster provisioning VM.

Console: Products and Services > Compute Engine > VM instances
Click on [Create instance]

| Property      | Value               |
| Name:         | management          |
| Zone:         | your-choice         |
| Machine type: | micro (shared vCPU) |

You shouldn't need to change the following settings, just verify them.

| Boot disk:       | New 10GB, Debian Linux                 |
| Service account: | Compute Engine default service account |
| Firewall:        | don't change                           |

- Click on the extended menu at the bottom to access advanced features.
- Click on the Disks tab to get advanced options
- Uncheck the the box before Delete boot disk when instance is deleted.

Click Create

### Authorize the VM to use the Cloud SDK

- SSH to the VM.

This VM is going to use API calls to create the Hadoop cluster. So it needs to have the Google Cloud SDK installed and it needs to be authorized to use the API.
The SDK is required for the gcloud command line tool. So we can verify that the Google Cloud SDK is installed if the gcloud tool is installed. The -v (version) option will list the Google Cloud SDK version.
Check if the SDK is installed on this VM:
```
$ gcloud -v
Google Cloud SDK 309.0.0
alpha 2020.09.03
beta 2020.09.03
bq 2.0.60
core 2020.09.03
gsutil 4.53
kubectl 1.15.11
```
Now to see if the permissions are set up to make calls to the Google Cloud, let's try a simple gcloud command that will use the API:
```
$ gcloud compute zones list
ERROR: (gcloud.compute.zones.list) Some requests did not succeed:
 - Insufficient Permission: Request had insufficient authentication scopes.
```
Copy the JSON credentials to the VM.

1. Find and open the downloaded JSON file in a text editor. The file was downloaded to your computer in Step 1.
2. Select all the text and copy it. (Usually CTRL-A and CTRL-C).
3. On management in the SSH terminal, create a file named credentials.json.

Paste the contents of the JSON file into this new file on the VM.

4. Authorize the VM
Use the gcloud auth command and the credentials file you just created to authorize the VM.
```
$ gcloud auth activate-service-account --key-file credentials.json
Activated service account credentials for: [management@<project>.gserviceaccount.com]
```
Re-initialize gcloud, which will now display the services account you created
```
$ gcloud init
Welcome! This command will take you through the configuration of gcloud.
Settings from your current configuration [default] are:
core:
  account: management@hadoop-287506.iam.gserviceaccount.com
  disable_usage_reporting: 'True'
  project: hadoop-287506
Pick configuration to use:
 [1] Re-initialize this configuration [default] with new settings 
 [2] Create a new configuration
Please enter your numeric choice:  1
Your current configuration has been set to: [default]
You can skip diagnostics next time by using the following flag:
  gcloud init --skip-diagnostics
Network diagnostic detects and fixes local network connection issues.
Checking network connection...done.                                                                                                                                                                    
Reachability Check passed.
Network diagnostic passed (1/1 checks passed).
Choose the account you would like to use to perform operations for 
this configuration:
 [1] 797717275386-compute@developer.gserviceaccount.com
 [2] management@hadoop-287506.iam.gserviceaccount.com
 [3] Log in with a new account
Please enter your numeric choice:  2
You are logged in as: [management@hadoop-287506.iam.gserviceaccount.com].
API [cloudresourcemanager.googleapis.com] not enabled on project 
[797717275386]. Would you like to enable and retry (this will take a 
few minutes)? (y/N)?  y
Enabling service [cloudresourcemanager.googleapis.com] on project [797717275386]...
Operation "operations/acf.d6f94d5e-82d3-4dac-92de-5983ad4c9c71" finished successfully.
Pick cloud project to use: 
 [1] hadoop-287506
 [2] Create a new project
Please enter numeric choice or text value (must exactly match list 
item):  1
Your current project has been set to: [hadoop-287506].
Do you want to configure a default Compute Region and Zone? (Y/n)?  
Which Google Compute Engine zone would you like to use as project 
default?
If you do not specify a zone via a command line flag while working 
with Compute Engine resources, the default is assumed.
 [1] us-east1-b
 [2] us-east1-c
 [3] us-east1-d
 [4] us-east4-c
 [5] us-east4-b
 [6] us-east4-a
 [7] us-central1-c
 [8] us-central1-a
 [9] us-central1-f
 [10] us-central1-b
 [11] us-west1-b
 [12] us-west1-c
 [13] us-west1-a
 [14] europe-west4-a
 [15] europe-west4-b
 [16] europe-west4-c
 [17] europe-west1-b
 [18] europe-west1-d
 [19] europe-west1-c
 [20] europe-west3-c
 [21] europe-west3-a
 [22] europe-west3-b
 [23] europe-west2-c
 [24] europe-west2-b
 [25] europe-west2-a
 [26] asia-east1-b
 [27] asia-east1-a
 [28] asia-east1-c
 [29] asia-southeast1-b
 [30] asia-southeast1-a
 [31] asia-southeast1-c
  [32] asia-northeast1-b
 [33] asia-northeast1-c
 [34] asia-northeast1-a
 [35] asia-south1-c
 [36] asia-south1-b
 [37] asia-south1-a
 [38] australia-southeast1-b
 [39] australia-southeast1-c
 [40] australia-southeast1-a
 [41] southamerica-east1-b
 [42] southamerica-east1-c
 [43] southamerica-east1-a
 [44] asia-east2-a
 [45] asia-east2-b
 [46] asia-east2-c
 [47] asia-northeast2-a
 [48] asia-northeast2-b
 [49] asia-northeast2-c
 [50] asia-northeast3-a
Did not print [24] options.
Too many options [74]. Enter "list" at prompt to print choices fully.
Please enter numeric choice or text value (must exactly match list 
item):  1
Your project default Compute Engine zone has been set to [us-east1-b].
You can change it by running [gcloud config set compute/zone NAME].
Your project default Compute Engine region has been set to [us-east1].
You can change it by running [gcloud config set compute/region NAME].
Created a default .boto configuration file at [/home/arendina13/.boto]. See this file and
[https://cloud.google.com/storage/docs/gsutil/commands/config] for more
information about configuring Google Cloud Storage.
Your Google Cloud SDK is configured and ready to use!
* Commands that require authentication will use management@hadoop-287506.iam.gserviceaccount.com by default
* Commands will reference project `hadoop-287506` by default
* Compute Engine commands will use region `us-east1` by default
* Compute Engine commands will use zone `us-east1-b` by default
Run `gcloud help config` to learn how to change individual settings
This gcloud configuration is called [default]. You can create additional configurations if you work with multiple accounts and/or projects.
Run `gcloud topic configurations` to learn more.
Some things to try next:
* Run `gcloud --help` to see the Cloud Platform services you can interact with. And run `gcloud help COMMAND` to get help on any gcloud command.
* Run `gcloud topic --help` to learn about advanced features of the SDK like arg files and output formatting
```
Verify that the VM is now authorized to use Google Cloud Platform API. Let's see what zones are available
```
$ gcloud compute zones list
```
This time the command should succeed.
Delete credentials.json from the VM.
```
$ rm credentials.json
```

## Customize the VM

You will customize the VM and install necessary software. The final step is to create the Hadoop cluster using the customized VM to verify that it is working.
We are going to be using an open source cluster automation tool called bdutil. The code is open source and is hosted in a Git repository. To copy that code to the VM, you first need to install Git.

## Install wget

```
$ sudo apt-get install wget
```

## Install Ansible

```
sudo apt install ansible

```

## Install Git

```
sudo apt install git
```

## Deploy cluster infrastructure

The infrastructure will be composed by 1 master and 3 workers.

Open provisioning/deploy.sh and change the variables as needed.


## Configure password-less SSH

On management machine:

```
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/user/.ssh/id_rsa.
Your public key has been saved in /home/user/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:xsAbawdj7ZkKzVYMZacbPdrJMxYYOpFsmriD4Nk3g1o user@management
The key's randomart image is:
+---[RSA 2048]----+
|     .oo+ .      |
|     .+B *       |
|   . +X B +      |
|. . o+ % O +     |
|o.o.o B S B      |
|.ooE B + . o     |
|  o.. +          |
| .               |
|                 |
+----[SHA256]-----+

$ cat .ssh/id_rsa.pub >> .ssh/authorized_keys
```

## References

- https://codelabs.developers.google.com/codelabs/gcp-infra-cloud-api-infrastructure-automation/index.html?index=..%2F..cloud#0
- https://linuxize.com/post/install-java-on-debian-10/
